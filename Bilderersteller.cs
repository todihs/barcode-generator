﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;



namespace Barcodegenerator
{
    class Bilderersteller
    {
        private Bitmap Bild;
        private int Bildbreite;
        private int Bildhöhe;
        private Form1 dieForm;
        //---Konstrucktor---
        public Bilderersteller(Form1 übergabe)
        {
            Form1 dieForm = übergabe;
        }

        //---Erzeugung---
        public void Bilderzeugen(int x, int y)
        {
            Bildbreite = x;
            Bildhöhe = y;
            Bild = new Bitmap(Bildbreite, Bildhöhe);
        }

        //---Bildbemahlung---
        public void Flächeerzeugen(int xa, int x, int y, Color Farbe)
        {
            for (int i = xa; i < x; i++)
            {
                for (int b = 0; b < y; b++)
                {
                    Bild.SetPixel(i, b, Farbe);
                }
            }
        }



        //---BildAusgaben---
        public Bitmap BildAusgabe()
        {
            return Bild;
        }

        public void BilddateiSpeichern(string filename)
        {
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            userName = userName.Remove(0, userName.LastIndexOf(@"\") + 1);
            Bild.Save(@"C:\Users\" + userName + @"\Desktop\" + filename + ".jpg");
        }

    }
}
