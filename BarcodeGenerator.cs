﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


namespace Barcodegenerator
{
    class BarcodeGenerator
    {
        private Bilderersteller derBilderersteller;
        Form1 dieForm;
        private int Breite = 0;
        private int Höhe = 75;
        private string code;

        //---Konstrucktor---
        public BarcodeGenerator(Form1 übergabe) 
        {
            Form1 dieForm = übergabe;
            derBilderersteller = new Bilderersteller(dieForm);
        }
        public void Nullsetzen()
        {
            Breite = 0;
        }

        //---erzeugen des Bildes und hintergrund weiseln---
        public void Barcodeerzeugen() 
        {
            derBilderersteller.Bilderzeugen(Breite, Höhe);
            derBilderersteller.Flächeerzeugen(0, Breite, Höhe, Color.White);
        }

        public void Barcodeberechnen(string code)
        {
            this.code = code;
            char[] codeabfolge = code.ToCharArray();
            char[] Vergleichstabelle = {'0','1','2','3','4','5','6','7','8','9','B','U','-'};
            string[] Decodiertebuchstaben = { "NnNwWnWnN", "WnNwNnNnW", "NnWwNnNnW", "WnWwNnNnN", "NnNwWnNnW", "WnNwWnNnN", "NnWwWnNnN", "NnNwNnWnW", "WnNwNnWnN", "NnWwNnWnN", "NnWnNwNnW", "WwNnNnNnW","NwNnNnWnW" };
            string Decodiertertext = "";

            //dieForm.Staturnachricht(Zeichenfolde[1]);

            for (int i = 0; i < codeabfolge.Length; i++)
            {
                for (int b = 0; b < 13; b++ )
                {
                    if (codeabfolge[i] == Vergleichstabelle[b])
                    {
                        Decodiertertext = Decodiertertext + Decodiertebuchstaben[b];
                    }
                }
            }
                     
            Bildwandler(Decodiertertext);
        }

        //---umwandelung von Formalzeichen N,n,W,w in in bilddatei
        private void Bildwandler(string Decodiertertext)
        {
            char[] textbildwandlung = Decodiertertext.ToCharArray();
            char [] zeichen = {'N','n','W','w'};
            int xa = 0;
            int x = 0;


            for (int b = 0; b < textbildwandlung.Length; b++)
            {
                if(textbildwandlung[b] == zeichen[2] | textbildwandlung[b] == zeichen[3])
                {
                    Breite = Breite + 4;
                }
                if (textbildwandlung[b] == zeichen[0] | textbildwandlung[b] == zeichen[1])
                {
                    Breite = Breite + 2;
                }
            }

            Barcodeerzeugen();

            for (int i = 0; i < textbildwandlung.Length; i++)
            {
                if (xa > Breite)
                {
                    dieForm.Staturnachricht("Bild_zu_breit");
                }

                if(textbildwandlung[i] == zeichen[0]) //N
                {
                    x = xa + 2;
                    derBilderersteller.Flächeerzeugen(xa, x, Höhe, Color.Black);
                    xa = xa + 2;
                }
                if(textbildwandlung[i] == zeichen[1]) //n
                {
                    x = xa + 2;
                    derBilderersteller.Flächeerzeugen(xa, x, Höhe, Color.White);
                    xa = xa + 2;
                }
                if(textbildwandlung[i] == zeichen[2]) //W
                {
                    x = xa + 4;
                    derBilderersteller.Flächeerzeugen(xa, x, Höhe, Color.Black);
                    xa = xa + 4;
                }
                if (textbildwandlung[i] == zeichen[3]) //w
                {
                    x = xa + 4;
                    derBilderersteller.Flächeerzeugen(xa, x, Höhe, Color.White);
                    xa = xa + 4;
                }
            }

        }

        public void BarcodeSpeichern()
        {
            derBilderersteller.BilddateiSpeichern(code);
        }

    }
}
