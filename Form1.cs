﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Barcodegenerator
{
    public partial class Form1 : Form
    {
        private BarcodeGenerator derBarcode;
        private TextIO derTextIO;
        public Form1()
        {
            InitializeComponent();
            derBarcode = new BarcodeGenerator(this);
            derTextIO = new TextIO();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            derBarcode.Barcodeberechnen(edtCode.Text);
            derBarcode.Nullsetzen();

            derBarcode.BarcodeSpeichern();
        }
        public void Staturnachricht(String Ausgabetext)
        {
            lblAusgabe.Text = Ausgabetext;
        }
    }
}
